<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Cabang;
use App\Models\Divisi;
use RealRashid\SweetAlert\Facades\Alert;

//class string random
use Illuminate\Support\Str;

class UserController extends Controller
{
    public $role_item = [
        'admin',
        'eksekutif',
        'supervisor',
        'staff'
    ];

    public function index() {
        // $users = User::latest()->paginate(5);
        return view('home.pages.user.index', [
            'users' => User::latest()->FilterUser(request(['search']))->paginate(10)
        ]);
        // dd($user->cabang->name);
    }

    public function create() {
        $cabangs = Cabang::all();
        $divisis = Divisi::all();
        return view('home.pages.user.create', ['cabangs' => $cabangs, 'divisis' => $divisis]);
    }

    public function store(Request $request) {
        // dd($request->all());
        try {
            // |unique:App\Models\User,name
            // |unique:App\Models\User,username
            $validateData = $request->validate([
                'name' => 'required|string|max:255|unique:App\Models\User,name|max:255',
                'username' => 'required|string|unique:App\Models\User,username|max:30',
                'password' => 'required|string|max:255',
                'role' => 'required|in:admin,supervisor,staff,eksekutif',
                'divisi' => 'required|integer'
            ]);
            // dd($validateData);
            if ($validateData == FALSE) {
                return redirect('user')
                    ->withErrors($validateData)
                    ->withInput();
            }
    
            $user = new User();
            $user->name = strip_tags($request->name);
            $user->username = strip_tags($request->username);
            $user->password = bcrypt($request->password);
            $user->role = $request->role;
            $user->divisi_id = $request->divisi;
            // dd($user);
            $user->remember_token = Str::random(60) ;
            $user->save();
            Alert::success('Berhasil', 'Berhasil menambahkan user');
    
            return redirect()->route('user.index')->send();
        } catch (\Throwable $th) {
            // throw $th;
            Alert::error('Gagal', 'Gagal menyimpan user ');
            return redirect()->route('user.index')->send();
        }
    }

    public function delete($id)
    {
        try {
            $user = User::find($id);
            $user->delete();
            Alert::success('Berhasil', 'Berhasil menghapus user');
    
            return redirect()->route('user.index');
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal', 'Gagal menghapus user');
            return redirect()->route('user.index')->send();
        }
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        $cabangs = Cabang::where('id', '!=', $user->divisi->cabang_id)->get();
        $divisis = Divisi::where('id', '!=', $user->divisi_id)
                        ->where('cabang_id', $user->divisi->cabang->id)->get();
        if (($key = array_search($user->role, $this->role_item)) !== false) {
            unset($this->role_item[$key]);
        }
        // dd($this->role_item);
        return view('home.pages.user.edit', ['user' => $user,
                                            'cabangs' => $cabangs,
                                            'divisis' => $divisis,
                                            'roles' => $this->role_item]);
    }

    public function update(Request $request)
    {
        try {
            // |unique:App\Models\User,name,'.$request->id
            // |unique:App\Models\User,username,'.$request->id
            $validateData = $request->validate([
                'name' => 'required|string|max:255|unique:App\Models\User,name,'.$request->id,
                'username' => 'required|string|max:30|unique:App\Models\User,username,'.$request->id,
                'new_password' => 'nullable|string|max:255',
                'role' => 'required|in:admin,supervisor,staff,eksekutif',
                'divisi' => 'required|integer'
            ]);
            // dd($validateData);
            if ($validateData == FALSE) {
                return redirect('user.index')
                    ->withErrors($validateData)
                    ->withInput();
            }
    
            $user = User::find($request->id);
            // dd($user);
            $user->name = strip_tags($request->name);
            $user->username = strip_tags($request->username);
            $user->role = $request->role;
            $user->divisi_id = $request->divisi;
    
            
            if ($request->new_password == NULL) {
                $user->password = $user->password;
            }
            else {
                $user->password = bcrypt($request->new_password);
            }
            $user->save();
            Alert::success('Berhasil', 'Berhasil mengedit user');
            return redirect()->route('user.index')->send();
        } catch (\Throwable $th) {
            // throw $th;
            Alert::error('Gagal', 'Gagal edit user!');
            return redirect()->route('user.index')->send();
        }
    }

}
