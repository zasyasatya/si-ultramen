<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Task;
use App\Models\Cabang;
use App\Models\Divisi;
use App\Models\Upload;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    public function index() {
        $users = User::all();
        $user_total = count($users);
        $divisis = Divisi::all();
        $divisi_total = count($divisis);
        $doc_total = Upload::count('file');
        
        if (auth()->user()->role == 'supervisor') {
            // $tasks = Task::where('user_id', auth()->user()->id)->get();
            $tasks = Task::where('user_id', auth()->user()->id)->FilterTask(request(['search', 'category', 'divisi']))->paginate(10);
        } else {
            $tasks = Task::latest()->FilterTask(request(['search', 'category', 'divisi']))->paginate(10);
        }
        $task_total = count($tasks);

        $current_bagian = auth()->user()->bagian_id;

        $current_role = auth()->user()->role;
        return view('home.pages.dashboard.index', ['user_total' => $user_total,
                                                    'tasks' => $tasks,
                                                    'task_total' => $task_total,
                                                    'divisis' => $divisis,
                                                    'divisi_total' => $divisi_total,
                                                    'doc_total' => $doc_total,
                                                    'current_bagian' => $current_bagian,
                                                    'current_role' => $current_role]);
    }

    public function updateProfile(Request $request)
    {
        try {
            $validateData = $request->validate([
                'name' => 'required|string|max:255|unique:App\Models\User,name,'.$request->id,
                'username' => 'required|string|max:30|unique:App\Models\User,username,'.$request->id,
                'new_password' => 'nullable|string|max:255'
            ]);
    
            if ($validateData == FALSE) {
                return redirect('dashboard')
                    ->withErrors($validateData)
                    ->withInput();
            }
    
            $user = User::find($request->id);
            // dd($user);
            $user->name = strip_tags($request->name);
            $user->username = strip_tags($request->username);
            
            if ($request->new_password == NULL) {
                $user->password = $user->password;
            }
            else {
                $user->password = bcrypt($request->new_password);
            }
    
            $user->save();
            
            // dd($user);
            Alert::success('Berhasil', 'Profil telah terupdate');
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal', 'Gagal edit user!');
        }
        return redirect()->route('dashboard')->send();
    }
}
