<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Divisi;

class DivisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            // SPI
            [
                'name' => 'Satuan Pengawas Internal',
                'kode' => 'SPI',
                'cabang_id' => 1
            ],
            [
                'name' => 'Peng.Adm.Keu',
                'kode' => 'PAK',
                'cabang_id' => 1
            ],
            [
                'name' => 'Peng.Teknik',
                'kode' => 'PT',
                'cabang_id' => 1
            ],

            // UMUM
            [
                'name' => 'Umum',
                'kode' => 'Um',
                'cabang_id' => 1
            ],
            [
                'name' => 'Personalia',
                'kode' => 'P',
                'cabang_id' => 1
            ],
            [
                'name' => 'Adm dan Kesekre',
                'kode' => 'KAK',
                'cabang_id' => 1
            ],
            [
                'name' => 'Pemb. dan Gudang',
                'kode' => 'KPG',
                'cabang_id' => 1
            ],
            [
                'name' => 'RT dan Perawatan',
                'kode' => 'KRP',
                'cabang_id' => 1
            ],
            [
                'name' => 'Pengamanan',
                'kode' => 'KRP',
                'cabang_id' => 1
            ],

            // PELAYANAN
            [
                'name' => 'Pelayanan',
                'kode' => 'KPln',
                'cabang_id' => 1
            ],
            [
                'name' => 'Kas',
                'kode' => 'KKs',
                'cabang_id' => 1
            ],
            [
                'name' => 'Akuntansi',
                'kode' => 'KAn',
                'cabang_id' => 1
            ],
            [
                'name' => 'Perencana Keuangan',
                'kode' => 'KPKn',
                'cabang_id' => 1
            ],
            [
                'name' => 'Penagihan',
                'kode' => 'KPgh',
                'cabang_id' => 1
            ],
            

            //Perencanaan Teknik
            [
                'name' => 'Perencana Teknik',
                'kode' => 'KPTn',
                'cabang_id' => 1
            ],
            [
                'name' => 'Pengawasan',
                'kode' => 'KPsn',
                'cabang_id' => 1
            ],
           

            // Produksi
            [
                'name' => 'Produksi',
                'kode' => 'KPdk',
                'cabang_id' => 1
            ],
            [
                'name' => 'ME',
                'kode' => 'KsME',
                'cabang_id' => 1
            ],
            [
                'name' => 'Laboratorium',
                'kode' => 'KsLbt',
                'cabang_id' => 1
            ],
           

            // Distribusi
            [
                'name' => 'Distribusi',
                'kode' => 'KPDtb',
                'cabang_id' => 1
            ],
            [
                'name' => 'Meter Segel',
                'kode' => 'KsMS',
                'cabang_id' => 1
            ],
            

            // Litbang
            [
                'name' => 'Litbang',
                'kode' => 'KLb',
                'cabang_id' => 1
            ],
            [
                'name' => 'TI',
                'kode' => 'KsTI',
                'cabang_id' => 1
            ],
            [
                'name' => 'Pengembangan Usaha',
                'kode' => 'KsPU',
                'cabang_id' => 1
            ],
            
            // Gerokgak
            [
                'name' => 'Gerokgak',
                'kode' => 'KacG',
                'cabang_id' => 2
            ],
           
            // Busungbiu
            [
                'name' => 'Busungbiu',
                'kode' => 'KacBsb',
                'cabang_id' => 3
            ],
           
            // Seririt
            [
                'name' => 'Seririt',
                'kode' => 'KacSrt',
                'cabang_id' => 4
            ],
            
            // Lovina
            [
                'name' => 'Lovina',
                'kode' => 'KacLvn',
                'cabang_id' => 5
            ],
            // Kubutambahan
            [
                'name' => 'Kubutambahan',
                'kode' => 'KacKbtn',
                'cabang_id' => 6
            ],
            [
                'name' => 'Direktur',
                'kode' => 'boss',
                'cabang_id' => 1
            ],
            
        ];
        Divisi::insert($data);
    }
}
