<?php

namespace App\Http\Controllers;

use App\Models\Divisi;
use App\Models\Cabang;
use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class DivisiController extends Controller
{

    public function index(Request $request) {
        return view('home.pages.divisi.index', [
            'divisis' => Divisi::latest()->FilterDivisi()->paginate(10)
        ]);
    }

    public function create() {
        $cabangs = Cabang::all();
        return view('home.pages.divisi.create', ["cabangs" => $cabangs]);
    }

    public function store(Request $request) {
        try {
            $divisi = new Divisi();
            $validateData = $request->validate([
                'name'=> 'required|string|unique:App\Models\Divisi,name|max:50',
                'kode' => 'required|string|unique:App\Models\Divisi,kode|max:20',
                'cabang' => 'required|integer'
            ]);
           
            $divisi->name = strip_tags($request->name);
            $divisi->kode = strip_tags($request->kode);
            $divisi->cabang_id = $request->cabang;
            $divisi->save();

            Alert::success('Berhasil', 'Berhasil menambahkan divisi');
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal', 'Gagal menambahkan divisi');
        }

        return redirect()->route('divisi.index');
    }

    public function delete($id) {
        try {
            $statusDivisi = User::where('divisi_id', $id)->get()->isNotEmpty();
            // dd($statusDivisi);
            if($statusDivisi != true) {
                $divisi = Divisi::where('id', $id)->first();
                $divisi->delete();
                Alert::success('Berhasil', 'Berhasil menghapus divisi');
            }
            else {
                Alert::error('Gagal!', 'Divisi ini digunakan oleh User!');
            }
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal!', 'Gagal menghapus Divisi!');
        }

        return redirect()->route('divisi.index');
    }

    public function edit($id) {
        $divisi = Divisi::find($id);
        $cabangs = Cabang::where('id', '!=', $divisi->cabang_id)->get();
        return view('home.pages.divisi.edit', ['divisi' => $divisi,
                                                'cabangs' => $cabangs]);
    }

    public function update(Request $request) {
        try {
            $validateData = $request->validate([
                'name'=> 'required|string|max:50|unique:App\Models\Divisi,name,'.$request->id,
                'kode' => 'required|string|max:20|unique:App\Models\Divisi,kode,'.$request->id,
                'cabang' => 'required|integer'
            ]);
    
            // dd($request->name, $request->kode);
            if ($validateData == FALSE) {
                return redirect('divisi')
                    ->withErrors($validateData)
                    ->withInput();
            }
    
            $divisi = Divisi::find($request->id);
            $divisi->kode = strip_tags($request->kode);
            $divisi->name = strip_tags($request->name);
            $divisi->cabang_id = $request->cabang;
            $divisi->save();
            Alert::success('Berhasil', 'Berhasil mengedit divisi');
        } catch (\Throwable $th) {
            //throw $th;
            Alert::success('Gagal', 'Gagal mengedit divisi');
        }

        return redirect()->route('divisi.index');
    }
}
