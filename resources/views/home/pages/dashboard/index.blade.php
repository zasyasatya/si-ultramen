@extends('home.layouts.app')
@section('title', 'Dashboard')

@section('custom_css')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/slick/slick.css' )}}"/>
@endsection

@section('content')
  @include('home.layouts.jumbotron')
  <div class="col-lg-12 col-md-12">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-12 col-6 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar alert alert-success d-flex justify-content-center align-items-center">
                <i class="menu-icon tf-icons bx bx-task rounded m-auto d-block mb-10 position-absolute"></i>
              </div>
              <div class="dropdown">
                <button
                  class="btn p-0"
                  type="button"
                  id="cardOpt3"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  
                </button>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt3">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold mt-n3 d-block mb-1">Penugasan</span>
            <h3 class="card-title mb-2">{{ $task_total }}</h3>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-6 col-sm-12 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar alert alert-info d-flex justify-content-center align-items-center">
                  <i class="menu-icon tf-icons bx bx-user rounded m-auto d-block mb-10 position-absolute"></i>
              </div>
              <div class="dropdown">
                <button
                  class="btn p-0"
                  type="button"
                  id="cardOpt6"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  
                </button>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt6">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold d-block mt-n3 mb-1">User</span>
            <h3 class="card-title text-nowrap mb-1">{{ $user_total }}</h3>
            <!-- <small class="text-success fw-semibold"><i class="bx bx-up-arrow-alt"></i> +28.42%</small> -->
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-6 col-sm-12 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar alert alert-danger d-flex justify-content-center align-items-center">
                <i class="menu-icon tf-icons bx bx-briefcase rounded m-auto d-block mb-10 position-absolute"></i>
              </div>
              <div class="dropdown">
                <button
                  class="btn p-0"
                  type="button"
                  id="cardOpt4"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  
                </button>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt4">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold d-block mt-n3 mb-2">Divisi</span>
            <h3 class="card-title text-nowrap mb-2">{{ $divisi_total }}</h3>
            <!-- <small class="text-danger fw-semibold"><i class="bx bx-down-arrow-alt"></i> -14.82%</small> -->
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-6 col-sm-12 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar alert alert-primary d-flex justify-content-center align-items-center">
                <i class="menu-icon tf-icons bx bx-file rounded m-auto d-block mb-10 position-absolute"></i>
              </div>
              <div class="dropdown">
                <button
                  class="btn p-0"
                  type="button"
                  id="cardOpt1"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  
                </button>
                <div class="dropdown-menu" aria-labelledby="cardOpt1">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold d-block mt-n3 mb-1">Dokumen</span>
            <h3 class="card-title mb-2">{{ $doc_total }}</h3>
            <!-- <small class="text-success fw-semibold"><i class="bx bx-up-arrow-alt"></i> +28.14%</small> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md">
      <!-- <h5 class="">Daftar Penugasan</h5> -->

      <div id="carouselExample" class="carousel slide carousel-dark mt-n4" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="row my-4 mx-10 slider">
              @foreach ($divisis as $key => $divisi) 
              <?php $taskActive = App\Models\Divisi::taskCheck($divisi->id)?>
                  @if ($taskActive)
                    {{-- @if ((($current_role == 'supervisor') and ($current_bagian == $divisi->tasks->first()->user->bagian->id)) or ($current_bagian == 1 or $current_bagian == 2)) --}}
                      <div class="col-md-6 col-lg-3 col-sm-12 mb-4 px-3">
                        <div class="card h-100">
                          <div class="card-header d-flex align-items-center justify-content-between">
                            <div class="alert-sm alert-secondary my-auto" role="alert">
                              <h7 class="card-title m-0">{{ $divisi->name }}</h7>
                            </div> 
                          </div>
                          <div class="card-body">
                            <div class="list-group">
                              <ul class="p-0 m-0">
                                @foreach ($divisi->tasks as $task)
                                  <a href="{{ route('task.detail', ['id' => $task->id]) }}">
                                    <li class="d-flex justify-align-center list-group-item">
                                      <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                        <div class="me-2">
                                          <small class="text-muted d-block mb-1">expired: {{ $task->expired_date }}</small>
                                          <h7 class="mb-0 text-truncate">{{ \Illuminate\Support\Str::limit($task->name, 20) }}</h7>
                                        </div>
                                        <div class="user-progress justify-align-center justify-content-center d-flex gap-1 my-auto">
                                          <div class="avatar-sm rounded-circle alert {{ $task->status == 'uploaded' || $task->status == 'done' ? 'alert-success' : 'alert-danger'}} d-flex justify-content-center align-items-center my-auto">
                                            <i class="menu-icon tf-icons bx {{ $task->status == 'uploaded' || $task->status == 'done' ? 'bx-check' : 'bx-x'}} rounded m-auto d-block mb-10 position-absolute"></i>
                                        </div>
                                        </div>
                                      </div>
                                    </li>  
                                  </a>
                                @endforeach
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--/ card-penugasan -->
                    {{-- @endif --}}
                  @endif   
              @endforeach
            </div>  
          </div>
        </div>
        <a class="carousel-control-prev left-arrow" href="#carouselExample" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </a>
        <a class="carousel-control-next right-arrow" href="#carouselExample" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </a>
      </div>
    </div>
</div>
@endsection

@section('custom_js')
  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="{{ asset('assets/slick/slick.min.js') }}"></script>
@endsection

@section('custom_slick')
  <script type="text/javascript">
    $(document).ready(function(){
      $('.slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $(".left-arrow"),
        nextArrow: $(".right-arrow"),
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
            }
          },
          {
            breakpoint: 900,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 700,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }]
      });
    });
  </script>
@endsection