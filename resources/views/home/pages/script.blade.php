@section('javascript_statusCabang')
<script>
  $(function() {
    $(function() {
      $('#status_cabang').on('change', function() {
        let cabang_id = $("#status_cabang").val();
        // console.log(cabang_id);
        $.ajax({
            type: "POST",
            url:"{{route('task.list_divisi')}}",
            data: {
              "_token": "{{ csrf_token() }}",
              "cabang_id":cabang_id
            },
            cache: false,
            success:function(res) {
              // console.log(res)
              $('#divisi').html(res);
            },
            error:function(data) {
              console.log('error pada', data)
            }
          });
      })
    })
  });

</script>
@endsection