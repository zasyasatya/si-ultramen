<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Divisi;

class Task extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'name',
        'expired_date',
        'status',
        'message',
        'divisi_id',
        'user_id'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function divisi() {
        return $this->belongsTo(Divisi::class);
    }

    public function upload() {
        return $this->hasOne(Upload::class);
    }


    public function scopeFilterTask($query, array $filters){
        $query->when($filters['search'] ?? false, function($query, $search){
            // if (auth()->user()->role == 'supervisor') {
            //     return $query->where('user_id', auth()->user()->id)
            //                  ->whereHas('divisi', function($query) use ($search) {
            //                     $query->where('tasks.name', 'LIKE', '%' .request('search'). '%')
            //                             ->orwhere('tasks.expired_date', 'LIKE', '%' .$search. '%')
            //                             ->orwhere('divisis.name', 'LIKE', '%' .$search. '%');
            //                  });  
            // }
            // else {

            // return $query->whereHas('divisi', function($query) use ($search) {
            //                 $query->where('tasks.name', 'LIKE', '%' .request('search'). '%')
            //                         ->orwhere('tasks.expired_date', 'LIKE', '%' .$search. '%')
            //                         ->orwhere('divisis.name', 'LIKE', '%' .$search. '%');
            //             });

            return $query->whereHas('divisi', function($query) use ($search) {
                            $query->where('tasks.name', 'LIKE', '%' .request('search'). '%')
                                    ->orwhere('tasks.expired_date', 'LIKE', '%' .$search. '%')
                                    ->orwhere('divisis.name', 'LIKE', '%' .$search. '%');
                        })
                        ->orWhereHas('user', function($query) use ($search) {
                            $query->where('users.name', 'LIKE', '%' .request('search'). '%');
                        })
                        ->orWhereHas('divisi.cabang', function($query) use ($search) {
                            $query->where('cabangs.name', 'LIKE', '%' .$search. '%');
                        })
                        ;

            // }
        });

        $query->when($filters['category'] ?? false, function($query, $category){
            return $query->where('status', $category)->get(); 
        });
    }
}

