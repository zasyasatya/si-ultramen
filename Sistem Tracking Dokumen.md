## Sistem Tracking Dokumen
Sistem yang mentracking dokumen laporan tiap cabang

### Problem
- Berbagai macam dokumen (berupa file hardcopy) dilaporkan dari setiap cabang secara fisik.
- Menyebabkan sering terjadi kesulitan dalam pencarian dokumen yang lampau atau kehilangan data.

### DB
#### `document`
- `id`
- `name`
- `expired_date`
- `id_cabang`
  - kepada siapa dokumen ini ditugaskan untuk diupload
- `status` 
  - ('uploaded', 'not_uploaded', 'revise', 'done')

#### `document_access`
- `id`
- `id_document`
- `id_role`
- `id_cabang`

#### `upload`
- `id`
- `id_dokumen`
- `id_user`
  - pemilik file / pengunggah file
- `name`
- `file`

#### `user`
- `id`
- `name`
- `xxx`
- `id_cabang`
- `id_role`

#### `cabang`
- `id`
- `name`
- `cabang`

#### `role`
- `id`
- `name`

### User Interface
- Dashboard (Supervisor)
  - Rekap yang sudah dan belum diupload di bulan tertentu
- Daftar Dokumen (Cabang)
  - List semua dokumen berdasarkan tugas yang diterima
    - List di Klik, muncul halaman upload dan history file2 yang telah diupload berdasarkan satu task tersebut
- Tugas (Admin)
  - List Tugas yang *dibuat oleh SPI* kepada cabang
- Cabang (Admin)
  - List Cabang
- User(Admin)