<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'file',
        'message',
        'date',
        'task_id',
        'user_id'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function task() {
        return $this->belongsTo(Task::class);
    }

    public function uploadOwner() {
        return $this->hasOneThrough(User::class, Task::class);
    }

    public static function getFile($task_id) {
        return Upload::select('file', 'date')->where('task_id', $task_id)->get();
    }

    public function scopeFilterUpload($query){
        // if(Request('search')){
        //     if (auth()->user()->role == 'staff') {
        //         return $query->where('user_id', auth()->user()->id)
        //                      ->where('name', 'LIKE', '%' .request('search'). '%')
        //                      ->orWhere('status', 'LIKE', '%' .request('search'). '%');  
        //     }
        //     else {
        //         return $query->where('name', 'LIKE', '%' .request('search'). '%')
        //                      ->orWhere('status', 'LIKE', '%' .request('search'). '%'); 
        //     }
        // }

        if(Request('search')){
                return $query->where('name', 'LIKE', '%' .request('search'). '%');
                            //  ->orWhere('status', 'LIKE', '%' .request('search'). '%');  
            
            
        }
    }

}
