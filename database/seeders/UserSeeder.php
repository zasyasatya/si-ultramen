<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Bapak Super Admin',
                'username' => 'spi1', 
                'role' => 'admin',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 1,
            ],
            [
                'name' => 'Ibu Umum',
                'username' => 'umum', 
                'role' => 'supervisor',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 4,
            ],
            [
                'name' => 'Kakak Pelayanan',
                'username' => 'pelayanan', 
                'role' => 'supervisor',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 10,

            ],
            [
                'name' => 'Kakak Perencana Teknik',
                'username' => 'teknik', 
                'role' => 'supervisor',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 15,

            ],
            [
                'name' => 'Kakak Produksi',
                'username' => 'produksi', 
                'role' => 'supervisor',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 17,

            ],
            [
                'name' => 'Kakak Distribusi',
                'username' => 'distribusi', 
                'role' => 'supervisor',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 20,

            ],
            [
                'name' => 'Kakak Gerokgak',
                'username' => 'gerokgak', 
                'role' => 'staff',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 25,

            ],
            [
                'name' => 'Kakak Busungbiu',
                'username' => 'busungbiu', 
                'role' => 'staff',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 26,

            ],
            [
                'name' => 'Kakak Seririt',
                'username' => 'seririt', 
                'role' => 'staff',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 27,

            ],
            [
                'name' => 'Kakak Lovina',
                'username' => 'lovina', 
                'role' => 'staff',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 28,

            ],
            [
                'name' => 'Kakak Kubutambahan',
                'username' => 'kubutambahan', 
                'role' => 'staff',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 29,

            ],
            [
                'name' => 'Pak Boss',
                'username' => 'boss', 
                'role' => 'eksekutif',
                'password' => bcrypt('123'),
                'remember_token' => Str::random(60),
                'divisi_id' => 30,
        
            ],

        ];
        
        User::insert($data);
    }
}
