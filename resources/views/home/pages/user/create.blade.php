@extends('home.layouts.app')
@section('title', 'User')

@section('content')
<div class="col-lg-12 col-md-12">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-12 mb-4">
        <!-- Basic Layout -->
        <div class="col-xxl">
          <div class="card mb-4">
            <div class="card-header d-flex align-items-center justify-content-between">
              <h5 class="mb-0">Tambah User</h5>
            </div>
            <div class="card-body">
              <form action={{ route('user.store') }} method="POST">
                {{ csrf_field() }}
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan nama" name="name" autocomplete="name" />
                    @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @error('username') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan username" name="username" autocomplete="username" />
                    @error('username')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan password" name="password" />
                    @error('password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Role</label>
                  <div class="col-sm-10">
                    <select id="cars" name="role" class="form-select @error('role') is-invalid @enderror" >
                      <option value="">-- Role User --</option>
                      <option value="admin">admin</option>
                      <option value="eksekutif">eksekutif</option>
                      <option value="supervisor">supervisor</option>
                      <option value="staff">staff</option>
                    </select>
                    @error('role')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Cabang</label>
                  <div class="col-sm-10">
                    <select id="status_cabang" name="" class="form-select @error('cabang') is-invalid @enderror">
                      <option value="">-- Cabang User --</option>
                      @foreach ($cabangs as $cabang)
                          <option value="{{ $cabang->id }}">{{ $cabang->name }}</option> 
                      @endforeach
                    </select>
                    @error('cabang')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Divisi</label>
                  <div class="col-sm-10">
                    <select id="divisi" name="divisi" class="form-select @error('divisi') is-invalid @enderror" >
                      <option value="">-- divisi User --</option>
                      @foreach ($divisis as $divisi)
                        <option value="{{ $divisi->id }}">{{ $divisi->name }}</option>  
                      @endforeach
                    </select>
                    @error('divisi')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row justify-content-end">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Send</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@include('home.pages.script')