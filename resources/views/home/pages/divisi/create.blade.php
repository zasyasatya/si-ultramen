@extends('home.layouts.app')
@section('title', 'divisi')

@section('content')
<div class="col-lg-12 col-md-12 col-12 mb-4">
    <!-- Basic Layout -->
    <div class="col-xxl">
      <div class="card mb-4">
        <div class="card-header d-flex align-items-center justify-content-between">
          <h5 class="mb-0">Tambah divisi</h5>
        </div>
        <div class="card-body">
        <form action={{ route('divisi.store') }} method="POST">
            {{ csrf_field() }}
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label" for="basic-default-name">Nama divisi</label>
              <div class="col-sm-10">
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan nama divisi" name="name"/>
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label" for="basic-default-company">Cabang</label>
              <div class="col-sm-10">
                <select id="status_cabang" onChange="getValue()" name="cabang" class="form-select @error('cabang') is-invalid @enderror" >
                  <option value="">-- Cabang --</option>
                  @foreach ($cabangs as $cabang)
                      <option value="{{ $cabang->id }}">{{ $cabang->name }}</option> 
                  @endforeach
                </select>
                @error('cabang')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label" for="basic-default-company">Kode divisi</label>
              <div class="col-sm-10">
                <input type="text" class="form-control @error('kode') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan kode divisi" name="kode"/>
                @error('kode')
                  <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="row justify-content-end">
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Send</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection