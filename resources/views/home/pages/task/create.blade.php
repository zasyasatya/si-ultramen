@extends('home.layouts.app')
@section('metaHeader')
  <meta name="csrf_token" content=" {{ csrf_token() }}"/>
@endsection
@section('title', 'Penugasan')

@section('content')
  <div class="col-lg-12 col-md-12">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-12 mb-4">
        <!-- Basic Layout -->
        <div class="col-xxl">
          <div class="card mb-4">
            <div class="card-header d-flex align-items-center justify-content-between">
              <h5 class="mb-0">Tambah Penugasan</h5>
            </div>
            <div class="card-body">
              <form action="{{ route('task.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Nama Dokumen</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan nama dokumen" name="name"/>
                    @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Cabang Tujuan</label>
                  <div class="col-sm-10">
                    <select id="status_cabang" name="cabang" class="form-select @error('cabang') is-invalid @enderror">
                      <option>-- Nama Cabang --</option>
                      @foreach ($cabangs as $cabang)
                      <option value="{{ $cabang->id }}">{{ $cabang->name }}</option>
                      @endforeach
                    </select>
                    @error('cabang')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Divisi Tujuan</label>
                  <div class="col-sm-10">
                    <select id="divisi" name="divisi" class="form-select @error('divisi') is-invalid @enderror">
                      <option>-- Nama Divisi --</option>
                      {{-- @foreach ($divisis as $divisi)
                      <option value="{{ $divisi->id }}">{{ $divisi->name }}</option>
                      @endforeach --}}
                    </select>
                    @error('divisi')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-email">tenggat waktu</label>
                  <div class="col-sm-10">
                    <div class="input-group input-group-merge">
                      <input
                        type="date"
                        id="basic-default-date"
                        class="form-control @error('expired_date') is-invalid @enderror"
                        placeholder="date"
                        aria-label="date"
                        name="expired_date"
                      />
                      @error('expired_date')
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">pesan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control @error('message') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan pesan penugasan" name="message"></textarea>
                    @error('message')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row justify-content-end">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@include('home.pages.script')