<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'kode',
    ];

    public function scopeFilterCabang($query){
        if(Request('search')){
           return $query->where('name', 'LIKE', '%' .request('search'). '%') 
                    ->orWhere('kode', 'LIKE', '%' .request('search'). '%');
        }
    }

    // public function users() {
    //     return $this->hasMany(User::class);
    // }

    // public function tasks() {
    //     return $this->hasMany(Task::class);
    // }

    // public function tasks() {
    //     return $this->belongsTo(Task::class);
    // }

    public function divisi() {
        return $this->belongsToMany(Divisi::class);
    }

    public static function taskCheck($cabang_id) {
        return Task::where('cabang_id', $cabang_id)->get()->isNotEmpty();
    }

    public static function printSelected($status_cabang) {
        dd($status_cabang);
    }
    public static function statusCheck($status_cabang) {
        // dd($status_cabang);
        return Divisi::where('level', $status_cabang == 1 ? 'kabag' : 'kacab')
                            ->orWhere('level', $status_cabang == 1 ? 'kasubag' : 'kasi')
                            ->orWhere('level', $status_cabang == 1 ? 'other_pusat' : 'other_nonPusat')
                            ->get();
        // return dd($status_cabang);
    }

    public static function show($id) {
        return Cabang::where('id', '!=', $id)->get();
    }
}
