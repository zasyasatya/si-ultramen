<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('name');
            $table->enum('role', ['admin', 'supervisor', 'staff', 'eksekutif']);
            $table->string('username', 30);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            // relational to cabang and divisi
            $table->unsignedInteger('divisi_id'); 
            $table->foreign('divisi_id')
            ->references('id')
            ->constrained()
            ->on('divisis')
            ->cascadeOnDelete()
            ->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
