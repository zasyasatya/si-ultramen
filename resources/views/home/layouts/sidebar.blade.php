<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
      <img src="{{asset('assets/img/icons/perumda.png')}}" alt="" width="120" class="rounded mx-auto d-block">
    </div>

    <!-- <div class="menu-inner-shadow"></div> -->

    <ul class="menu-inner py-3">
        @can('isAdmin')
            <li class="menu-item py-1 {{ (request()->is('dashboard')) ? 'active' : '' }}">
                <a href="{{route('dashboard')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
                </a>
            </li>
        
            <li class="menu-item py-1 {{ (request()->routeIs('cabang.*')) ? 'active' : '' }}">
                <a href="{{route('cabang.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-briefcase"></i>
                <div data-i18n="Analytics">Cabang</div>
                </a>
            </li>

            <li class="menu-item py-1 {{ (request()->routeIs('divisi.*')) ? 'active' : '' }}">
                <a href="{{route('divisi.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-columns"></i>
                <div data-i18n="Analytics">Divisi</div>
                </a>
            </li>
        
            <li class="menu-item py-1 {{ (request()->routeIs('user.*')) ? 'active' : '' }}">
                <a href="{{route('user.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="Analytics">User</div>
                </a>
            </li>

            <li class="menu-item py-1 {{ (request()->routeIs('task.*')) ? 'active' : '' }}">
                <a href="{{route('task.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-task"></i>
                <div data-i18n="Analytics">Penugasan</div>
                </a>
            </li>
            
        @elsecan('isSupervisor')
            <li class="menu-item py-1 {{ (request()->routeIs('dashboard')) ? 'active' : '' }}">
                <a href="{{route('dashboard')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
                </a>
            </li>
        
            <li class="menu-item py-1 {{ (request()->routeIs('task.*')) ? 'active' : '' }}">
                <a href="{{route('task.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-task"></i>
                <div data-i18n="Analytics">Penugasan</div>
                </a>
            </li>
        @elsecan('isStaff')
            <li class="menu-item py-1 {{ (request()->is('upload')) ? 'active' : '' }}">
                <a href="{{route('upload.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-upload"></i>
                <div data-i18n="Analytics">Upload</div>
                </a>
            </li> 
        @elsecan('isEksekutif')
            <li class="menu-item py-1 {{ (request()->is('dashboard')) ? 'active' : '' }}">
                <a href="{{route('dashboard')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
                </a>
            </li>

            <li class="menu-item py-1 {{ (request()->routeIs('cabang.*')) ? 'active' : '' }}">
                <a href="{{route('cabang.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-briefcase"></i>
                <div data-i18n="Analytics">Cabang</div>
                </a>
            </li>

            <li class="menu-item py-1 {{ (request()->routeIs('divisi.*')) ? 'active' : '' }}">
                <a href="{{route('divisi.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-columns"></i>
                <div data-i18n="Analytics">Divisi</div>
                </a>
            </li>
        
            <li class="menu-item py-1 {{ (request()->routeIs('user.*')) ? 'active' : '' }}">
                <a href="{{route('user.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="Analytics">User</div>
                </a>
            </li>
        
            <li class="menu-item py-1 {{ (request()->routeIs('task.*')) ? 'active' : '' }}">
                <a href="{{route('task.index')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-task"></i>
                <div data-i18n="Analytics">Penugasan</div>
                </a>
            </li>
        @endcan
    </ul>
  </aside>