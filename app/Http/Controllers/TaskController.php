<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Cabang;
use App\Models\Divisi;
use App\Models\Bagian;
use App\Models\Task;
use App\Models\Upload;
use RealRashid\SweetAlert\Facades\Alert;
//class string random
use Illuminate\Support\Str;

class TaskController extends Controller
{
    public $role_item = [
        'admin',
        'eksekutif',
        'supervisor',
        'staff'
    ];

    //current bagian from user
    // public 

    public function index() {
        $tasks = Task::latest()->FilterTask(request(['search', 'category', 'divisi']))->paginate(10);

        $selectedCategory = request(['category']);
        $selectedCategory = implode(", ", $selectedCategory);
        
        return view('home.pages.task.index', [
            'tasks' => $tasks,
            'selectedCategory' => $selectedCategory
        ]);
    }

    public function create() {
        $divisis = Divisi::all();
        $cabangs = Cabang::all();
        return view('home.pages.task.create', ['divisis' => $divisis,
                                                'cabangs' => $cabangs]);
    }

    public function store(Request $request) {
        // dd($request->all());
        $validateData = $request->validate([
            'name' => 'required|string|unique:App\Models\Task,name|max:255',
            'divisi' => 'required|integer',
            'expired_date' => 'required',
            'message' => 'required'
        ]);

        if ($validateData == FALSE) {
            return redirect('task')
                ->withErrors($validateData)
                ->withInput();
        }

        $task = new Task();
        $task->user_id = auth()->user()->id;
        $task->name = strip_tags($request->name);
        $task->divisi_id = $request->divisi;
        $task->expired_date = $request->expired_date;
        $task->status = 'not_uploaded';
        $task->message = strip_tags($request->message);
        $task->save();

        Alert::success('Berhasil', 'Berhasil menambahkan penugasan');
        return redirect()->route('task.index')->send();
    }

    public function delete($id)
    {
        $task = Task::find($id);
        $task->delete();

        Alert::success('Berhasil', 'Berhasil menghapus penugasan');
        return redirect()->route('task.index');
    }

    public function edit($id)
    {
        $task = Task::where('id', $id)->first();
        $cabangs = Cabang::where('id', '!=', $task->divisi->cabang->id)->get();

        $divisis = Divisi::where('id', '!=', $task->divisi_id)
                            ->where('cabang_id', $task->divisi->cabang->id)->get();
        // dd($cabangs);
        return view('home.pages.task.edit', ['task' => $task,
                                            'cabangs' => $cabangs,
                                            'divisis' => $divisis]);
    }

    public function update(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:255|unique:App\Models\Task,name,'.$request->id,
            'divisi' => 'required|integer',
            'expired_date' => 'required',
            'message' => 'required'
        ]);

        if ($validateData == FALSE) {
            return redirect('task')
                ->withErrors($validateData)
                ->withInput();
        }

        $task = Task::find($request->id);
        // dd($task);
        $task->user_id = auth()->user()->id;
        $task->name = strip_tags($request->name);
        $task->divisi_id = $request->divisi;
        $task->expired_date = $request->expired_date;
        $task->message = strip_tags($request->message);
        $task->save();
        
        Alert::success('Berhasil', 'Berhasil mengedit penugasan');
        return redirect()->route('task.index');
    }

    public function detail($id) {
        $task = Task::find($id);

        $currentRole = auth()->user()->role;

        $uploads = Upload::where('task_id', $id)->get();
        return view('home.pages.task.detail', ['task' => $task,
                                                'uploads' => $uploads,
                                                'currentRole' => $currentRole]);
    }

    public function done($id) {
        $task = Task::find($id);
        $task->status = 'done';
        $task->save();

        $currentBagian = auth()->user()->bagian_id;

        $uploads = Upload::where('task_id', $id)->get();
        // Alert::info('Penugasan Selesai', 'Penugasan telah ditandai selesai');
        Alert::success('Berhasil', 'Penugasan telah ditandai selesai');
        
        return redirect()->route('task.detail', ['id' => $task->id]);
    }

    public function revise(Request $request) {
        $task = Task::find($request->id);
        $task->status = 'revise';
        $task->message = strip_tags($request->message);
        $task->save();
        // dd($task);

        $uploads = Upload::where('task_id', $request->id)->get();
        $currentBagian = auth()->user()->bagian_id;
        Alert::success('Berhasil', 'Penugasan telah di-assign revisi');

        return redirect()->route('task.detail', ['id' => $task->id]);
    }

    public function download($url) {
        $file = Upload::find($url)->file;
        // dd($file);
        return Storage::download($file);
    }

    public function listDivisi(request $request) {
        
        $divisis = Divisi::where('cabang_id', $request->cabang_id)->get();

        foreach($divisis as $divisi) {
            echo "<option value='$divisi->id'>$divisi->name</option>";
        }

        // $divisis = Divisi::where('cabang_id', $id)->get();
        // return dd($request->cabang_id);
    }
}
