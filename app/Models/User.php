<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

use App\Models\Cabang;
use App\Models\Divisi;
use App\Models\Upload;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'role',
        'password',
        'divisi_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function cabang() {
        return $this->belongsTo(Cabang::class);
    }

    public function divisi() {
        return $this->belongsTo(Divisi::class);
    }
    
    public function upload() {
        return $this->belongsTo(Upload::class);
    }

    public static function show($id) {
        return User::find($id);
    }

    public function scopeFilterUser($query, array $filters){
        $query->when($filters['search'] ?? false, function($query, $search){
            return $query->whereHas('divisi', function($query) use ($search) {
                            $query->where('divisis.name', 'LIKE', '%' .$search. '%');
                        })
                        ->orWhereHas('divisi.cabang', function($query) use ($search) {
                            $query->where('cabangs.name', 'LIKE', '%' .$search. '%');
                        })
                        ->orwhere('users.name', 'LIKE', '%' .$search. '%')
                        ->orwhere('users.username', 'LIKE', '%' .$search. '%')
                        ->orWhere('role', 'LIKE', '%' .$search. '%');
        });
    }
}
