<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
Use App\Models\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Pemasangan Air Bulan Juli',  
                'expired_date' => '2022-10-17',
                'status' => 'not_uploaded',
                'message' => 'lengkapi dengan foto',
                'divisi_id' => 2,
                'user_id' => 2
            ],
            [
                'name' => 'Pemasangan Air Bulan September',  
                'expired_date' => '2022-10-17',
                'status' => 'not_uploaded',
                'message' => 'lengkapi dengan foto',
                'divisi_id' => 2,
                'user_id' => 2
            ],
            [
                'name' => 'Pemasangan Air Bulan Oktober',  
                'expired_date' => '2022-10-17',
                'status' => 'not_uploaded',
                'message' => 'lengkapi dengan foto',
                'divisi_id' => 3,
                'user_id' => 3
            ],
            [
                'name' => 'Pemasangan Air Bulan November',  
                'expired_date' => '2022-10-17',
                'status' => 'not_uploaded',
                'message' => 'lengkapi dengan foto',
                'divisi_id' => 3,
                'user_id' => 3
            ]
        ];
        Task::insert($data);
    }
}
