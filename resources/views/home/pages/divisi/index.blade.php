@extends('home.layouts.app')
@section('title', 'divisi')

@section('content')
      <div class="col-lg-12 col-md-12">
        <!-- search -->
        <div class="row justify-content-center">
          <div class="col-lg-12">
            <form action="{{ route('divisi.index') }}" method="GET">
              <div class="input-group form-group mb-3">
                <input type="text" class="form-control form-control-lg" placeholder="Search.." name="search" value="{{ request('search') }}">
                <button class="btn btn-primary" type="submit">Search</button>
              </div>
            </form>
          </div>
        </div>
        <!--/ search -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-12 mb-4">
            <!-- Striped Rows -->
            <div class="card">
              <div class="col-lg-12 d-flex align-items-center justify-content-between">
                <div>
                  <h5 class="card-header">Daftar divisi</h5>
                </div>
                @canany('isAdmin')
                  <div class="justify-content-end pe-4">
                    <a href="{{route('divisi.create')}}" class="btn btn-outline-primary">Add divisi</a>
                  </div>
                @endcanany
              </div>
              <div class="table-responsive text-nowrap">
                <table class="table table-striped">
                  @if ($divisis->count())
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode divisi</th>
                      <th>Nama divisi</th>
                      <th>Nama cabang</th>
                      @canany('isAdmin')
                        <th>Actions</th>
                      @endcanany
                      </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                      @foreach ($divisis as $key => $divisi)
                        <tr>
                          <td>{{ $key+1 }}</td>
                          <td>{{ $divisi->kode }}</td>
                          <td class="text-capitalize">{{ $divisi->name }}</td>
                          <td class="text-capitalize">{{ $divisi->cabang->name }}</td>
                          @canany('isAdmin')
                            <td>
                              <div class="dropdown">
                                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                                  <i class="bx bx-dots-vertical-rounded"></i>
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{ route('divisi.edit', ['id' => $divisi->id]) }}"
                                    ><i class="bx bx-edit-alt me-1"></i> Edit</a
                                  >
                                  <a class="dropdown-item" href="javascript:void(0);"
                                  data-bs-toggle="modal"
                                  data-bs-target="#deleteModal_{{$divisi->id}}"
                                  ><i class="bx bx-trash me-1"></i> Delete</a
                                  >
                                </div>
                              </div>
                            </td>
                            @endcanany
                        </tr>
                        <div class="col-lg-4 col-md-6">
                          <div>
                            <!-- Modal -->
                            <div class="modal fade" id="deleteModal_{{$divisi->id}}" tabindex="-1" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="modalCenterTitle">Apakah anda yakin ingin menghapus divisi ini?</h5>
                                    <button
                                      type="button"
                                      class="btn-close"
                                      data-bs-dismiss="modal"
                                      aria-label="Close"
                                    ></button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col mb-3 justify-content-center align-items-center">
                                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
                                          Batalkan
                                        </button>
                                        <a href="{{ route('divisi.delete', ['id' => $divisi->id]) }}">
                                          <button type="button" class="btn btn-primary">Hapus</button>
                                        </a>
                                      </div>
                                    </div> 
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    </tbody>
                  @else 
                    <p class="text-center">Nothing Found :(</p>
                  @endif
                </table>
              </div>
            </div>
            <!--/ Striped Rows -->
          </div>
        </div>
      </div>

      {{ $divisis->links() }}

@endsection