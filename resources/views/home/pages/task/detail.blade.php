@extends('home.layouts.app')
@section('title', 'Penugasan')

@section('content')
<div class="col-lg-12 col-md-12 col-12 mb-4">
    <div class="card"> 
            <div class="col-lg-12 d-flex align-items-center justify-content-between">
              <div>
                <h5 class="card-header">Detail Penugasan</h5>
              </div>
              <div class="justify-content-end pe-4">
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <form>
                  <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="basic-default-name">Penugasan Dokumen</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="basic-default-name"  value="{{ $task->name }}" readonly/>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="basic-default-name">Pemberi Tugas</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="basic-default-name"  value="{{ $task->user->name }}" readonly/>
                  </div>
                  </div>
                  <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="basic-default-company">Cabang tujuan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="basic-default-name" value="{{ $task->divisi->cabang->name }}" readonly/>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="basic-default-company">Divisi tujuan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="basic-default-name" value="{{ $task->divisi->name }}" readonly/>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="basic-default-email">tenggat waktu</label>
                    <div class="col-sm-10">
                      <div class="input-group input-group-merge">
                        <input type="text" class="form-control" id="basic-default-name" value="{{ $task->expired_date }}" readonly/>
                      </div>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="basic-default-name">pesan</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan pesan penugasan" readonly>{{ $task->message }}</textarea>
                    </div>
                  </div>
                  {{-- Button muncul ketika file sudah di-upload --}}
                  @if ((($task->status == 'uploaded') or ($task->status == 'revise')) and (($currentRole == 'supervisor') && (auth()->user()->id == $task->user->id)))
                    <div class="row mb-3">
                      <div class="demo-inline-spacing d-flex flex-row-reverse">
                        <div class="mx-1">
                          <a href="" class="btn btn-success" href="javascript:void(0);"
                            data-bs-toggle="modal"
                            data-bs-target="#doneModal_{{$task->id}}"
                            >
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                              <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                            </svg>
                            Selesai</a>
                        </div>
                        <div class="mx-1">
                          <a href="" class="btn btn-warning" href="javascript:void(0);"
                            data-bs-toggle="modal"
                            data-bs-target="#reviseModal_{{$task->id}}"  
                            >
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-counterclockwise" viewBox="0 0 16 16">
                              <path fill-rule="evenodd" d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"/>
                              <path d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z"/>
                            </svg>
                            Revisi</a>
                        </div>
                      </div>
                    </div>
                  @endif
                </form>  
              </div>   
            </div>
    </div>
    <!-- Striped Rows -->
    <div class="card mt-3">
      <div class="col-lg-12 d-flex align-items-center justify-content-between">
        <div>
          <h5 class="card-header">File Dokumen</h5>
        </div>
      </div>
      <div class="table-responsive text-nowrap">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>File</th>
              <th>Penggungah</th>
              <th>Catatan</th>
              <th>Upload at</th>
            </tr>
          </thead>
          <tbody class="table-border-bottom-0">
            @if ($task->status != 'not_uploaded')
              @foreach ($uploads as $key => $upload)
                <tr>
                  <td>{{ $key+1 }}</td>
                  <td>
                      <a href="{{ asset('storage/' .  $upload->file) }}"  target="_blank"><button type="button" class="btn rounded-pill btn-primary">lihat</button></a>
                      <a href="{{ route('task.download', ['url' => $upload->id])}}"><button type="button" class="btn rounded-pill btn-success">Unduh</button></a>
                  </td>
                  <td>
                    {{ $upload->user->name }}
                  </td>
                  <td>
                    {{$upload->message}}
                  </td>
                  <td>
                    {{ $upload->date }}
                  </td>
                </tr>
              @endforeach
            @else
                
            @endif
            <div class="col-lg-4 col-md-6"> 
                <div class="mt-3">
                  <!-- Modal -->
                  <div class="modal fade" id="reviseModal_{{$task->id}}" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form action="{{ route('task.revise') }}" method="POST">  
                        {{ csrf_field() }}
                        @method('put')
                            <input type="hidden" name="id" value="{{ $task->id }}">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel1">Pemberian Revisi</h5>
                              <button
                                type="button"
                                class="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                              ></button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col">
                                  <label for="nameBasic" class="form-label">Pesan Revisi</label>
                                  <textarea class="form-control " id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan pesan penugasan" name="message" required></textarea>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                                Close
                              </button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="mt-3">
                  <!-- Modal -->
                  <div class="modal fade" id="doneModal_{{$task->id}}" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalCenterTitle">Apakah anda menandai selesai penugasan ini??</h5>
                          <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                          ></button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col mb-3 justify-content-center align-items-center">
                              <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
                                Batalkan
                              </button>
                              <a href="{{ route('task.done', ['id' => $task->id]) }}">
                                <button type="button" class="btn btn-success">Selesai</button>
                              </a>
                            </div>
                          </div> 
                        </div> 
                      </div>
                    </div>
                  </div>
                </div>
                <div class="mt-3">
                  <!-- Modal -->
                  <div class="modal fade" id="accessReject" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalCenterTitle">Anda tidak memiliki akses melihat dokumen ini</h5>
                          <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                          ></button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col mb-3 justify-content-center align-items-center">
                              <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
                                Kembali
                              </button>
                              {{-- <a href="{{ route('task.delete', ['id' => $task->id]) }}">
                                <button type="button" class="btn btn-primary">Hapus</button>
                              </a> --}}
                            </div>
                          </div> 
                        </div> 
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </tbody>
        </table>
      </div>
    </div>
    <!--/ Striped Rows -->
</div>
@endsection