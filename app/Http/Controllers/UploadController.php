<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;
use App\Models\Upload;
use App\Models\Cabang;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class UploadController extends Controller
{
    public function index() {
        // $current_user = auth()->user()->id;
        // $current_cabang = Cabang::find(auth()->user()->cabang_id);

        $task_total = Task::where('divisi_id', auth()->user()->divisi_id)->count();
        $revise_total = Task::where('divisi_id', auth()->user()->divisi_id)
                        ->where('status', 'revise')
                        ->count();
        $done_total = Task::where('divisi_id', auth()->user()->divisi_id)
                        ->where('status', 'done')
                        ->count();

        $tasks = Task::where('divisi_id', auth()->user()->divisi_id)->FilterTask(request(['search', 'category']))->paginate(10);

        $selectedCategory = request(['category']);
        $selectedCategory = implode(", ", $selectedCategory);

        return view('home.pages.upload.index', ['tasks' => $tasks,
                                                'task_total' => $task_total,
                                                'revise_total' => $revise_total,
                                                'done_total' => $done_total,
                                                'selectedCategory' => $selectedCategory]);
    }

    public function store(Request $request) {
        try {
            //code....
            $validateData = $request->validate([
                'file' => 'required|file|mimes:doc,docx,pdf,xls,xlsx,jpg,bmp,png,jpeg|max:10000',
                // 'message' => 'mimetypes:text/plain'
            ]);
    
            if ($validateData == FALSE) {
                return redirect('upload')
                    ->withErrors($validateData)
                    ->withInput();
            }
    
            $upload = new Upload();
            $upload->file = $request->file('file')->store('file_doc');
            $upload->message = strip_tags($request->message);
            $upload->date = Carbon::now();
            $upload->task_id = $request->task_id;
            $upload->user_id = auth()->user()->id;
            $upload->save();
    
            $task = Task::find($request->task_id);
            $task->status = 'uploaded';
            $task->save();
    
            // dd($upload);
            
            Alert::success('Berhasil', 'Berhasil melakukan upload');
            return redirect()->route('upload.index')->send();
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal', 'Gagal upload File!');
            return redirect()->route('upload.index')->send();
        }
    }
}
