## SI-Ultramen
## _Sistem Informasi dan Tracking Dokumen_

SI-Ultramen merupakan Sistem Informasi yang berfungsi untuk memanajemen dokumen pada setiap divisi dan cabang pada Perumda Air Minum Tirta Hita Buleleng. Sistem Informasi ini menjadi media penugasan dokumen, upload dokumen, dan tracking dokumen.

## Features

- Dashboard informasi, progress upload dokumen, dan tracking dokumen
- Manajemen data user berdasarkan role, cabang, dan divisi
- Manajemen data divisi perusahaan
- Manajemen data cabang perusahaan
- Manajemen penugasan (pemberian tugas, upload, revisi, dan tracking)


## Tech

- [Laravel 9.20](https://laravel.com/) - Framework PHP
- [PHP 8.1+](https://www.php.net/) - Basis Bahasa Pemrograman yang digunakan
- [Sneat](https://github.com/themeselection/sneat-bootstrap-html-laravel-admin-template-free) - Template dalam pengembangan website
- [Slick.js](https://kenwheeler.github.io/slick/) - Framework Carousel
- [Sweet Alert](https://sweetalert2.github.io/) - Framwork alert 


## Installation for Local Server

Pastikan anda telah menambahkan database dengan nama "db_ultramenv2". Selanjutnya clone project melalui perintah berikut.
```sh
git clone https://gitlab.com/zasyasatya/si-ultramen.git
```
Selanjutnya jalankan migration dan seeder agar website siap untuk dioperasikan.
```sh
php artisan migrate:fresh --seed
```
> Note: `php artisan storage:link` diperlukan untuk upload dan view dokumen

## Developer

**Zasya; Deon**

