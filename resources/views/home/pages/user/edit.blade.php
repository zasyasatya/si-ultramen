@extends('home.layouts.app')
{{-- @extends('home.master') --}}
@section('title', 'User')

@section('content')
<div class="col-lg-12 col-md-12">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-12 mb-4">
        <!-- Basic Layout -->
        <div class="col-xxl">
          <div class="card mb-4">
            <div class="card-header d-flex align-items-center justify-content-between">
              <h5 class="mb-0">Edit User</h5>
            </div>
            <div class="card-body">
              <form action={{ route('user.update') }} method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$user->id}}">
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control text-capitalize @error('name') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan nama" name="name" value="{{$user->name}}" />
                    @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @error('username') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan username" name="username" value="{{$user->username}}" />
                    @error('username')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">New Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control @error('new_password') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan password baru" name="new_password" />
                    @error('new_password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Role</label>
                  <div class="col-sm-10">
                    <select id="cars" name="role" class="form-select @error('role') is-invalid @enderror" >
                      <option class="text-capitalize" value="{{ $user->role }}">{{ $user->role }}</option>
                      @foreach ($roles as $role)
                        <option class="text-capitalize" value="{{ $role }}">{{ $role }}</option>
                      @endforeach
                    </select>
                    @error('role')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Cabang</label>
                  <div class="col-sm-10">
                    <select id="status_cabang" name="cabang" class="form-select @error('cabang') is-invalid @enderror" >
                      <option class="text-capitalize" value="{{ $user->divisi->cabang->id }}">{{ $user->divisi->cabang->name }}</option>
                      @foreach ($cabangs as $cabang)
                          <option class="text-capitalize" value="{{ $cabang->id }}">{{ $cabang->name }}</option>  
                      @endforeach
                    </select>
                    @error('cabang')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-company">Divisi</label>
                  <div class="col-sm-10">
                    <select id="divisi" name="divisi" class="form-select @error('divisi') is-invalid @enderror" >
                      <option class="text-capitalize" value="{{ $user->divisi->id }}">{{ $user->divisi->name }}</option>
                      @foreach ($divisis as $divisi)
                        <option class="text-capitalize" value="{{ $divisi->id }}">{{ $divisi->name }}</option>  
                      @endforeach
                    </select>
                    @error('divisi')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="row justify-content-end">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Send</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@include('home.pages.script')