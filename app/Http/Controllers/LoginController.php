<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Cookie;

class LoginController extends Controller
{
    public function postlogin(Request $request) {
        // dd($request->all());
        $validateData = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($validateData == FALSE) {
            Alert::error('Error Title', 'Error Message');
            return redirect('/')
                ->withErrors($validateData)
                ->withInput();
        }     

        $credential = $request->only('username', 'password');
        $username = $request->username;
        $password = $request->password;
        $remember = $request->has('remember_me') ? true : false;
        // dd($remember);
        if (Auth::attempt($credential)) {
            if ($remember) {
                Cookie::queue('username_old', $username, 1440);
                Cookie::queue('password_old', $password, 1440);
            }
            else if (!$remember){
                Cookie::queue('username_old', $username, -1440);
                Cookie::queue('password_old', $password, -1440);
            }

            if(auth()->user()->role == 'staff') {
                alert()->success('Login berhasil', 'Selamat Datang!')->toToast();
                return redirect('/upload');
            }
            else {
                alert()->success('Login berhasil', 'Selamat Datang!')->toToast();
                return redirect('/dashboard');
            }
        }
        alert()->error('Login Gagal', 'Cek username dan password');
        return redirect('/');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
    }

}
