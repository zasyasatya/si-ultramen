@extends('home.layouts.app')
@section('title', 'User')

@section('content')
  <div class="col-lg-12 col-md-12">
    <!-- search -->
    <div class="row justify-content-center">
      <div class="col-lg-12">
        <form action="{{ route('user.index') }}">
            <div class="input-group form-group mb-3">
              <input type="text" class="form-control form-control-lg" placeholder="Search.." name="search" value="{{ request('search') }}">
              <button class="btn btn-primary" type="submit">Search</button>
            </div>
          </form>
        </div>
      </div>
      <!--/ search -->
      <div class="col-lg-12 col-md-12 col-12 mb-4">
        <!-- Striped Rows -->
        <div class="card">
          <div class="col-lg-12 d-flex align-items-center justify-content-between">
            <div>
              <h5 class="card-header">Daftar User</h5>
            </div>
            @canany('isAdmin')
              <div class="justify-content-end pe-4">
                <a href="{{route('user.create')}}" class="btn btn-outline-primary">Add User</a>
              </div>
            @endcanany
          </div>
          <div class="table-responsive text-nowrap">
            <table class="table table-striped">
              @if ($users->count())
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Cabang</th>
                    <th>Divisi</th>
                    @canany('isAdmin')
                      <th>Actions</th>
                    @endcanany
                  </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                  @foreach ($users as $key => $user)
                      <tr>
                        <td>{{ $key+1 }}</td>
                        <td class="text-capitalize">{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td class="text-capitalize">{{ $user->role }}</td>
                        <td class="text-capitalize">{{ $user->divisi->cabang->name }}</td>
                        <td class="text-capitalize">{{$user->divisi_id != null ? $user->divisi->name : '-'}}</td>
                        @canany('isAdmin')
                        <td>
                          <div class="dropdown">
                            <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                              <i class="bx bx-dots-vertical-rounded"></i>
                            </button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('user.edit', ['id' => $user->id]) }}"
                                  ><i class="bx bx-edit-alt me-1"></i> Edit</a
                                >
                                {{-- <a class="dropdown-item" href="javascript:void(0);"
                                  ><i class="bx bx-info-circle me-1"></i>Detail</a
                                > --}}
                                <a class="dropdown-item" href="javascript:void(0);"
                                data-bs-toggle="modal"
                                data-bs-target="#deleteModal_{{$user->id}}"
                                ><i class="bx bx-trash me-1"></i> Delete</a
                                >
                              </div>
                            </div>
                          </td>
                          @endcanany
                      </tr>
                      <div class="col-lg-4 col-md-6">
                        <div class="mt-3">
                          <!-- Modal -->
                          <div class="modal fade" id="deleteModal_{{$user->id}}" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="modalCenterTitle">Apakah anda yakin ingin menghapus User ini?</h5>
                                  <button
                                    type="button"
                                    class="btn-close"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  ></button>
                                </div>
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col mb-3 justify-content-center align-items-center">
                                      <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
                                        Batalkan
                                      </button>
                                      <a href="{{ route('user.delete', ['id' => $user->id]) }}">
                                        <button type="button" class="btn btn-primary">Hapus</button>
                                      </a>
                                    </div>
                                  </div> 
                                </div> 
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  @endforeach
                </tbody>
              @else 
                <p class="text-center">Nothing Found :(</p>
              @endif
            </table>
          </div>
        </div>
        <!--/ Striped Rows -->
      </div>
  </div>

  {{ $users->links() }}

@endsection
