<nav
    class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
    id="layout-navbar"
    >
    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
        <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
        <i class="bx bx-menu bx-sm"></i>
        </a>
    </div>

    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">

        <div class="navbar-nav align-items-center">
        <div class="nav-item d-flex align-items-center">
            <a href="{{ asset('assets/userGuide.pdf')}}" target="_blank">
                <button type="button" class="btn btn-outline-secondary">User Guide</button>
            </a>
        </div>
        </div>
        <!-- /Search -->

        <ul class="navbar-nav flex-row align-items-center ms-auto">
        <!-- Place this tag where you want the button to render. -->
        <!-- User -->
        <li class="nav-item navbar-dropdown dropdown-user dropdown">
            <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
           
            <div class="avatar avatar-online">
                <img src="{{asset('assets/img/avatars/1.png')}}" alt class="w-px-40 h-auto rounded-circle" />
            </div>
            </a>
            <ul class="dropdown-menu dropdown-menu-end">
            <li>
                <a class="dropdown-item" href="#">
                <div class="d-flex">
                    <div class="flex-shrink-0 me-3">
                    <div class="avatar avatar-online">
                        <img src="{{asset('assets/img/avatars/1.png')}}" alt class="w-px-40 h-auto rounded-circle" />
                    </div>
                    </div>
                    <div class="flex-grow-1">
                    <span class="fw-semibold d-block text-truncate">{{ auth()->user()->name }}</span>
                    <small class="text-muted">
                        @can('isAdmin')
                            Admin
                        @elsecan('isSupervisor')
                            Supervisor
                        @elsecan('isStaff')
                            Staff
                        @elsecan('isEksekutif')
                            Ekesekutif
                        @endcan
                    </small>
                    </div>
                </div>
                </a>
            </li>
            <li>
                <div class="dropdown-divider"></div>
            </li>
            <li>
                <a class="dropdown-item" href="javascript:void(0);" 
                    data-bs-toggle="modal"
                    data-bs-target="#profileModal_{{auth()->user()->id}}" 
                >
                <i class="bx bx-user me-2"></i>
                <span class="align-middle">My Profile</span>
                </a>
            </li>
            <li>
                <div class="dropdown-divider"></div>
            </li>
            <li>
                <a class="dropdown-item" href="{{ route('logout') }}">
                <i class="bx bx-power-off me-2"></i>
                <span class="align-middle">Log Out</span>
                </a>
            </li>
            </ul>
        </li>
        <!--/ User -->
        </ul>
    </div>
</nav>

<div class="col-lg-4 col-md-6"> 
    <div>
        <div class="modal fade" id="profileModal_{{ auth()->user()->id }}" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <?php $user = App\Models\User::show(auth()->user()->id)?>

                <form action="{{ route('home.updateProfile') }}" method="POST">  
                {{ csrf_field() }}
                @method('put')
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel1">My Profile</h5>
                      <button
                        type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                      ></button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col">
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="basic-default-name">Nama</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control text-capitalize @error('nameForUser') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan nama" name="name" value="{{ $user->name }}" />
                                  @error('nameForUser')
                                      <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="basic-default-name">Username</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control text-capitalize @error('username') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan username" name="username" value="{{ $user->username }}" />
                                  @error('username')
                                      <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="basic-default-name">New Password</label>
                                <div class="col-sm-10">
                                  <input type="password" class="form-control @error('new_password') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan password baru" name="new_password" />
                                  @error('new_password')
                                      <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Close
                      </button>
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
    </div>
</div>