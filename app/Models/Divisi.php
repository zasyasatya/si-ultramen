<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    use HasFactory;

    
    protected $fillable = [
        'id',
        'name',
        'kode', 
    ];

    public function scopeFilterDivisi($query){
        if(Request('search')){
        //    return $query->where('name', 'LIKE', '%' .request('search'). '%') 
        //             ->orWhere('kode', 'LIKE', '%' .request('search'). '%');
            $result = Divisi::whereHas('cabang', function ($query) {
                $query->where('divisis.name', 'LIKE', '%' .request('search'). '%')
                        ->orwhere('divisis.kode', 'LIKE', '%' .request('search'). '%')
                        ->orwhere('cabangs.name', 'LIKE', '%' .request('search'). '%');
            });
            return $result;
        }

    }

    public function user() {
        return $this->hasMany(User::class);
    }

    public function tasks() {
        return $this->hasMany(Task::class);
    }

    public function cabang() {
        return $this->belongsTo(Cabang::class);
    }

    // public static function statusCheck($status_cabang) {
    //     // dd($status_cabang);
    //     return Divisi::where('level', $status_cabang == 1 ? 'kabag' : 'kacab')
    //                         ->orWhere('level', $status_cabang == 1 ? 'kasubag' : 'kasi')
    //                         ->orWhere('level', $status_cabang == 1 ? 'other_pusat' : 'other_nonPusat')
    //                         ->get();
    //     // return dd($status_cabang);
    // }

    // public static function show($id) {
    //     return Cabang::where('id', '!=', $id)->get();
    // }

    public static function taskCheck($divisi_id) {
        // return Task::where('divisi_id', $divisi_id)->get()->isNotEmpty();
        $data = Task::where('divisi_id', $divisi_id)->get()->isNotEmpty();
        // dd($divisi_id, $data);
        return $data;
    }

    public static function show($id) {
        return Divisi::where('id', '!=', $id)->get();
    }
}
