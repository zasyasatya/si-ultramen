@extends('home.layouts.app')
@section('title', 'Cabang')

@section('content')
<div class="col-lg-12 col-md-12 col-12 mb-4">
    <!-- Basic Layout -->
    <div class="col-xxl">
      <div class="card mb-4">
        <div class="card-header d-flex align-items-center justify-content-between">
          <h5 class="mb-0">Tambah Cabang</h5>
        </div>
        <div class="card-body">
        <form action={{ route('cabang.store') }} method="POST">
            {{ csrf_field() }}
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label" for="basic-default-name">Nama Cabang</label>
              <div class="col-sm-10">
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan nama cabang" name="name"/>
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label" for="basic-default-company">Kode Cabang</label>
              <div class="col-sm-10">
                <input type="text" class="form-control @error('kode') is-invalid @enderror" id="basic-default-name" placeholder="Masukkan kode cabang" name="kode"/>
                @error('kode')
                  <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
         
            <div class="row justify-content-end">
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Send</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection