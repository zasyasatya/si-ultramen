@extends('home.layouts.app')
@section('title', 'upload')


@section('content')
@include('home.layouts.jumbotron') 
<div class="col-lg-12 col-md-12">
    <div class="row">
      <div class="col-lg-4 col-md-6 col-6 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar alert alert-primary d-flex justify-content-center align-items-center">
                <i class="menu-icon tf-icons bx bx-task rounded m-auto d-block mb-10 position-absolute"></i>
              </div>
              <div class="dropdown">
                <button
                  class="btn p-0"
                  type="button"
                  id="cardOpt3"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt3">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold mt-n3 d-block mb-1">Penugasan</span>
            <h3 class="card-title mb-2">
                  {{ $task_total }}
             </h3>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-6 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar alert alert-warning d-flex justify-content-center align-items-center">
                  <i class="menu-icon tf-icons bx bx-repeat rounded m-auto d-block mb-10 position-absolute"></i>
              </div>
              <div class="dropdown">
                <button
                  class="btn p-0"
                  type="button"
                  id="cardOpt6"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt6">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold d-block mt-n3 mb-1">Revisi</span>
            <h3 class="card-title text-nowrap mb-1">
              {{ $revise_total }}
            </h3>
            <!-- <small class="text-success fw-semibold"><i class="bx bx-up-arrow-alt"></i> +28.42%</small> -->
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-6 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar alert alert-success d-flex justify-content-center align-items-center">
                <i class="menu-icon tf-icons bx bx-check rounded m-auto d-block mb-10 position-absolute"></i>
              </div>
              <div class="dropdown">
                <button
                  class="btn p-0"
                  type="button"
                  id="cardOpt4"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt4">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold d-block mt-n3 mb-2">Selesai</span>
            <h3 class="card-title text-nowrap mb-2">
              {{ $done_total }}
            </h3>
            <!-- <small class="text-danger fw-semibold"><i class="bx bx-down-arrow-alt"></i> -14.82%</small> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- List penugasan untuk user ini -->
   <!-- search -->
   {{-- <small class="text-light fw-semibold text-center">SEARCH</small> --}}
   <div class="row justify-content-center">
     <div class="col-lg-12">
       <form action="{{ route('upload.index') }}" method=GET>
         <div class="input-group form-group mb-3 d-flex bd-highlight">
           <input type="text" class="form-control form-control-lg flex-grow-1 bd-highlight" placeholder="Search.." name="search" value="{{ request('search') }}">
           <select name="category" class="form-select bd-highlight" id="inputGroupSelect03" aria-label="Example select with button addon" style="width:10px">
              <option value="">All</option>
              <option value="{{ 'not_uploaded' }}" autocomplete="off" {{ $selectedCategory == 'not_uploaded' ? 'selected' : ''}}>Not Uploaded</option>
              <option value="{{ 'uploaded' }}" autocomplete="off" {{ $selectedCategory == 'uploaded' ? 'selected' : ''}} >Uploaded</option>
              <option value="{{ 'revise' }}" autocomplete="off" {{ $selectedCategory == 'revise' ? 'selected' : ''}} >Revise</option>
              <option value="{{ 'done' }}" autocomplete="off"{{ $selectedCategory == 'done' ? 'selected' : ''}} >Done</option>
            </select>
            <button class="btn btn-primary" type="submit">Search</button>
         </div>
       </form>
     </div>
   </div>
   <!--/ search -->
   @if ($tasks->count())
   <div class="col-md mb-4 mb-md-0">
       <small class="text-light fw-semibold">Daftar Penugasan</small>
       @foreach ($tasks as $key => $task)
        <div class="col-md">
         <div class="accordion mt-3" id="accordionExample">
           <div class="card accordion-item">
             <h2 class="accordion-header" id="heading_{{ $key }}">
               <button
                 type="button"
                 class="accordion-button collapsed"
                 data-bs-toggle="collapse"
                 data-bs-target="#accordion_{{ $key }}"
                 aria-expanded="false"
                 aria-controls="accordion_{{ $key }}"
               >
               <p class="mr-10 my-2 pr-10">
                 {{ $task->name }}
               </p>
               <div class="px-3">
                 @if ($task->status == 'not_uploaded')
                   <span class="ml-10 badge bg-label-danger">New Upload</span>
                 @elseif($task->status == 'revise')
                   <span class="ml-10 badge bg-label-warning">Revisi</span>
                 @elseif($task->status == 'done')
                   <span class="ml-10 badge bg-label-success">Selesai</span>
                 @elseif($task->status == 'uploaded')
                   <span class="ml-10 badge bg-label-info">Terupload</span>
                 @endif
               </div>
               </button>
             </h2>
   
             <div
               id="accordion_{{ $key }}"
               class="accordion-collapse collapse "
               data-bs-parent="#accordionExample"
             >
                 <div class="accordion-body">
                     <form action="{{ route('upload.store')}}" method="POST" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <input type="hidden" name="task_id" value="{{ $task->id }}">
                       <div class="row mb-3">
                        <label class="col-sm-2 col-form-label" for="basic-default-email"> tenggat waktu </label>
                          <div class="col-sm-10">
                              <div class="input-group input-group-merge">
                                <input type="text" class="form-control" id="basic-default-name" value="{{ $task->expired_date }}" readonly/>
                              </div>
                          </div>
                       </div>
                       <div class="row mb-3">
                        <label class="col-sm-2 col-form-label" for="basic-default-email"> pemberi penugasan </label>
                          <div class="col-sm-10">
                              <div class="input-group input-group-merge">
                                <input type="text" class="form-control" id="basic-default-name" value="{{ $task->user->name }} (Divisi {{ $task->user->divisi->name}} / {{ $task->user->divisi->cabang->name}})" readonly/>
                              </div>
                          </div>
                       </div>
                       <div class="row mb-3">
                           <label class="col-sm-2 col-form-label" for="basic-default-name">Pesan penugasan</label>
                           <div class="col-sm-10">
                               <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan pesan penugasan" readonly>{{ $task->message }}</textarea>
                           </div>
                       </div>
                       
                       {{-- model for several file --}}
                       <?php $files = App\Models\Upload::getFile($task->id) ?>
 
                       {{-- timeline --}}
                       @foreach ($files as $key => $file)   
                           <section class="timeline_area section_padding_130">
                             <div class="container">
                                 <div class="row justify-content-center">
                                     <div class="col-12 col-sm-8 col-lg-6">
                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-12">
                                         <!-- Timeline Area-->
                                         <div class="apland-timeline-area">
                                             <!-- Single Timeline Content-->
                                             <div class="single-timeline-area">
                                                 <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                                                 </div>
                                                 <div class="row">
                                                     <div class="col-12 col-md-12 col-lg-6 ">
                                                         <div class="single-timeline-content d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                                             <div class="timeline-icon justify-content-center align-items-center"><i class="menu-icon tf-icons bx bx-task rounded m-auto d-block mb-10 "></i></div>
                                                             <div class="timeline-text">
                                                                 <h6>{{ $file->date }}</h6>
                                                                 <div class="justify-content-end">
                                                                   <a href="{{ 'storage/' . $file->file }}" class="btn btn-primary" target="_blank">File {{ $key+1 }}</a>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                           </section>
                       @endforeach
                       @if (($task->status != 'done') and ($task->status != 'uploaded'))
                         <div class="row mb-3">  
                             <label for="formFile" class="col-sm-2 col-form-label">Upload Dokumen</label>
                             <div class="col-sm-10">
                                 <input class="form-control @error('file') is-invalid @enderror" type="file" id="formFile" name="file"/>
                                 @error('file')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                             </div>
                         </div>
 
                         <div class="row mb-3">
                             <label class="col-sm-2 col-form-label" for="basic-default-name">catatan</label>
                             <div class="col-sm-10">
                                 <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan catatan kepada Supervisor" name="message"></textarea>
                             </div>
                         </div>
                         <div class="row justify-content-end">
                           <div class="col-sm-10">
                               <button type="submit" class="btn btn-primary" {{ $task->status == 'uploaded' and 'done' ? 'disabled' : ''}}>Send</button>
                           </div>
                         </div>
                       @endif
                     </form>
                 </div>
             </div>
           </div>  
         </div>
       </div> 
       @endforeach
   </div>
       
   @else
       <p>Noting found :(</p>
   @endif
    <!-- /List penugasan untuk user ini -->
@endsection