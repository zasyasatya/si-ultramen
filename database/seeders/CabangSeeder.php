<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Cabang;

class CabangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Pusat',
                'kode' => 'pst',
            ],
            [
                'name' => 'Gerokgak',
                'kode' => 'ggk',
            ],
            [
                'name' => 'Busungbiu',
                'kode' => 'bsb',
            ],
            [
                'name' => 'Seririt',
                'kode' => 'srt',
            ],
            [
                'name' => 'Lovina',
                'kode' => 'lvn',
            ],
            [
                'name' => 'Kubutambahan',
                'kode' => 'kbt',
            ],
        ];
        Cabang::insert($data);
    }
}
