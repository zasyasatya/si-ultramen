@extends('home.layouts.app')
@section('title', 'Penugasan')

@section('content')
  <div class="col-lg-12 col-md-12">
       <!-- search -->
       <div class="row justify-content-center">
        <div class="col-lg-12">
          <form action="{{ route('task.index') }}">
            <div class="input-group form-group mb-3">
              <input type="text" class="form-control form-control-lg" placeholder="Search.." name="search" value="{{ request('search') }}" id="mySearch">
              <select name="category" class="form-select bd-highlight" id="inputGroupSelect03" aria-label="Example select with button addon" style="width:10px">
                <option value="">All</option>
                <option value="{{ 'not_uploaded' }}" autocomplete="off" {{ $selectedCategory == 'not_uploaded' ? 'selected' : ''}}>Not Uploaded</option>
                <option value="{{ 'uploaded' }}" autocomplete="off" {{ $selectedCategory == 'uploaded' ? 'selected' : ''}} >Uploaded</option>
                <option value="{{ 'revise' }}" autocomplete="off" {{ $selectedCategory == 'revise' ? 'selected' : ''}} >Revise</option>
                <option value="{{ 'done' }}" autocomplete="off"{{ $selectedCategory == 'done' ? 'selected' : ''}} >Done</option>
              </select>
              <button class="btn btn-primary" type="submit">Search</button>
            </div>
          </form>
        </div>
      </div>
      <!--/ search -->
      <div class="col-lg-12 col-md-12 col-12 mb-4">
       <!-- Striped Rows -->
        <div class="card">
          <div class="col-lg-12 d-flex align-items-center justify-content-between">
            <div>
              <h5 class="card-header">Daftar Penugasan</h5>
            </div>
            <div class="justify-content-end pe-4">
              @canany('isSupervisor')
                <a href="{{ route('task.create') }}" class="btn btn-outline-primary">Add Penugasan</a> 
              @endcanany
            </div>
          </div>
          <div class="table-responsive text-nowrap">
            <table id="myTable" class="table table-striped">
              @if ($tasks->count())
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Penugasan</th>
                    <th>Pemberi Penugasan</th>
                    <th>Cabang Tujuan</th>
                    <th>Divisi Tujuan</th>
                    <th>Tenggat waktu</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                  @foreach ($tasks as $key => $task)
                  <tr>
                      <td>{{ $key+1 }}</td>
                      <td></i> {{ $task->name }}</td>
                      <td></i> {{ $task->user->name }}</td>
                      <td>{{ $task->divisi->cabang->name }}</td>
                      <td>{{ $task->divisi->name }}</td>
                      <td>
                          {{ $task->expired_date}}
                      </td>
                      <td>
                          @if ($task->status == 'uploaded')
                              <span class="badge bg-label-info me-1">Uploaded</span>
                          @elseif ($task->status == 'not_uploaded')
                              <span class="badge bg-label-danger me-1">Pending</span>
                          @elseif ($task->status == 'revise')
                              <span class="badge bg-label-warning me-1">Revise</span>
                          @else 
                              <span class="badge bg-label-success me-1">Done</span>
                          @endif
                      </td>
                      <td>
                          <div class="dropdown">
                          <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                              <i class="bx bx-dots-vertical-rounded"></i>
                          </button>
                          <div class="dropdown-menu">
                            {{-- Check if the current user is supervisor and current user can edit/delete their task --}}
                            @if ((auth()->user()->id == $task->user->id) && (auth()->user()->role == 'supervisor')) 
                              <a class="dropdown-item" href="{{ route('task.edit', ['id' => $task->id]) }}"
                              ><i class="bx bx-edit-alt me-1"></i> Edit</a>
                              
                              <a class="dropdown-item" 
                              data-bs-toggle="modal"
                              data-bs-target="#deleteModal_{{ $task->id }}" 
                              href="javascript:void(0);"
                              ><i class="bx bx-trash me-1"></i> Delete</a>
                              <a class="dropdown-item" href="{{ route('task.detail', ['id' => $task->id]) }}"
                                ><i class="bx bx-info-circle me-1"></i>Detail</a>
                            @else
                              <a class="dropdown-item" href="{{ route('task.detail', ['id' => $task->id]) }}"
                              ><i class="bx bx-info-circle me-1"></i>Detail</a>
                            @endif
                          </div>
                          </div>
                      </td>
                  </tr>
                  <div class="col-lg-4 col-md-6">
                      <div class="mt-3">
                        <!-- Modal -->
                        <div class="modal fade" id="deleteModal_{{$task->id}}" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="modalCenterTitle">Apakah anda yakin ingin menghapus Penugasan ini?</h5>
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col mb-3 justify-content-center align-items-center">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
                                      Batalkan
                                    </button>
                                    <a href="{{ route('task.delete', ['id' => $task->id]) }}">
                                      <button type="button" class="btn btn-primary">Hapus</button>
                                    </a>
                                  </div>
                                </div> 
                              </div> 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endforeach
                </tbody>
              @else 
                <p class="text-center">Nothing Found :(</p>
              @endif 
            </table>
          </div>
        </div>
        <!--/ Striped Rows -->
      </div>
  </div>

  {{ $tasks->links()}}

@endsection
