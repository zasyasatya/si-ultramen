<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Cabang;
use App\Models\Divisi;

class CabangController extends Controller
{
    // strip_tags() => prevent script and html injection

    public $cabang_statusItem = [
        'pusat',
        'non_pusat'
    ];

    public function index() {
        return view('home.pages.cabang.index', [
            'cabangs' => Cabang::latest()->FilterCabang()->paginate(5)
        ]);
    }

    public function create() {
        return view('home.pages.cabang.create');
    }

    public function store(Request $request) {
        try {
            $validateData = $request->validate([
                'name'=> 'required|string|unique:App\Models\Cabang,name|max:50',
                'kode' => 'required|string|unique:App\Models\Cabang,kode|max:20',
            ]);
    
            if ($validateData == FALSE) {
                return redirect('cabang')
                    ->withErrors($validateData)
                    ->withInput();
            }
    
            $cabang = new Cabang();
            $cabang->name = strip_tags($request->name);
            $cabang->kode = strip_tags($request->kode);
            $cabang->save();
    
            Alert::success('Berhasil', 'Berhasil Menambahkan Cabang');
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal', 'Gagal Menambahkan Cabang');
        }
        return redirect()->route('cabang.index');

    }

    public function delete($id) {
        try {
            $statusCabang = Divisi::where('cabang_id', $id)->get()->isNotEmpty();
            // dd($statusCabang);
            if($statusCabang != true) {
                $cabang = Cabang::where('id', $id)->first();
                $cabang->delete();
                Alert::success('Berhasil', 'Berhasil menghapus cabang');
            }
            else {
                Alert::error('Gagal!', 'Cabang ini digunakan!');
            }
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal!', 'Gagal hapus cabang!');
        }
        return redirect()->route('cabang.index');
    }

    public function edit($id) {
        $cabang = Cabang::find($id);
        return view('home.pages.cabang.edit', ['cabang' => $cabang]);
    }

    public function update(Request $request) {
        try {
            $validateData = $request->validate([
                'name'=> 'required|string|max:50|unique:App\Models\Cabang,name,'.$request->id,
                'kode' => 'required|string|max:20|unique:App\Models\Cabang,kode,'.$request->id,
            ]);
    
            if ($validateData == FALSE) {
                return redirect('cabang')
                    ->withErrors($validateData)
                    ->withInput();
            }
    
            $cabang = Cabang::find($request->id);
            $cabang->kode = strip_tags($request->kode);
            $cabang->name = strip_tags($request->name);
            $cabang->save();
            Alert::success('Berhasil', 'Berhasil mengedit cabang');
        } catch (\Throwable $th) {
            //throw $th;
            Alert::error('Gagal', 'Gagal mengedit cabang');
        }
        return redirect()->route('cabang.index');
    }
}
