<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CabangController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\DivisiController;
use App\Http\Controllers\UploadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Route::post('/postlogin', [LoginController::class, 'postlogin'])->name('postlogin');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::group(['middleware' => ['auth', 'rolecheck:admin,supervisor,eksekutif']], function() { //page access require auth
    
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
    Route::put('/dashboard/update_profile', [HomeController::class, 'updateProfile'])->name('home.updateProfile');

    Route::prefix('user')->middleware(['auth', 'rolecheck:eksekutif,admin'])->group(function() {
        Route::get('/', [UserController::class, 'index'])->name('user.index');
        Route::get('create', [UserController::class, 'create'])->name('user.create');
        Route::post('store', [UserController::class, 'store'])->name('user.store');
        Route::get('edit/{id}', [UserController::class, 'edit'])->name('user.edit');
        Route::post('update', [UserController::class, 'update'])->name('user.update');
        Route::get('delete/{id}', [UserController::class, 'delete'])->name('user.delete');
        Route::get('list_divisi/{id}', [UserController::class, 'listDivisi'])->name('user.list_divisi');
    });
    
    Route::prefix('cabang')->middleware(['auth', 'rolecheck:eksekutif,admin'])->group(function() {
        Route::get('/', [CabangController::class, 'index'])->name('cabang.index');
        Route::get('create', [CabangController::class, 'create'])->name('cabang.create');
        Route::post('store', [CabangController::class, 'store'])->name('cabang.store');
        Route::get('edit/{id}', [CabangController::class, 'edit'])->name('cabang.edit');
        Route::post('update', [CabangController::class, 'update'])->name('cabang.update');
        Route::get('delete/{id}', [CabangController::class, 'delete'])->name('cabang.delete');
    });

    Route::prefix('divisi')->middleware(['auth', 'rolecheck:eksekutif,admin'])->group(function() {
        Route::get('/', [DivisiController::class, 'index'])->name('divisi.index');
        Route::get('create', [DivisiController::class, 'create'])->name('divisi.create');
        Route::post('store', [DivisiController::class, 'store'])->name('divisi.store');
        Route::get('edit/{id}', [DivisiController::class, 'edit'])->name('divisi.edit');
        Route::post('update', [DivisiController::class, 'update'])->name('divisi.update');
        Route::get('delete/{id}', [DivisiController::class, 'delete'])->name('divisi.delete');
    });

    Route::prefix('task')->middleware(['auth', 'rolecheck:supervisor,eksekutif,admin'])->group(function() {
        Route::get('/', [TaskController::class, 'index'])->name('task.index');
        Route::get('create', [TaskController::class, 'create'])->name('task.create');
        Route::post('store', [TaskController::class, 'store'])->name('task.store');
        Route::get('edit/{id}', [TaskController::class, 'edit'])->name('task.edit');
        Route::post('update', [TaskController::class, 'update'])->name('task.update');
        Route::get('detail/{id}', [TaskController::class, 'detail'])->name('task.detail');
        Route::get('delete/{id}', [TaskController::class, 'delete'])->name('task.delete');
        Route::get('done/{id}', [TaskController::class, 'done'])->name('task.done');
        Route::put('revise', [TaskController::class, 'revise'])->name('task.revise');
        Route::get('download/{url}', [TaskController::class, 'download'])->name('task.download');
        Route::post('list_divisi/', [TaskController::class, 'listDivisi'])->name('task.list_divisi');
    });

});

Route::group(['middleware' => ['auth', 'rolecheck:staff']], function() {
    Route::prefix('upload')->group(function() {
        Route::get('/', [UploadController::class, 'index'])->name('upload.index');
        Route::post('store', [UploadController::class, 'store'])->name('upload.store');
    });
});



